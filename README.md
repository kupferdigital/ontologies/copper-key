# Copper Key Ontology

The data from the [Copper Key](https://standalone.kupferschluessel.de/standalone.php?lang=english) is mapped against this ontology in this [ETL Workflow](https://gitlab.com/kupferdigital/workflows/copper-key-etl).

![copper-key-iof](https://kupferdigital.gitlab.io/ontologies/copper-key/copper-key-iof.svg)

There are two versions of this ontology:

1. [Copper-key](https://kupferdigital.gitlab.io/ontologies/copper-key/copper-key.svg): Has only the axioms needed for browsing the Copper Key Graph with [sparklis](https://github.com/sebferre/sparklis).
1. [Copper-key-iof](https://kupferdigital.gitlab.io/ontologies/copper-key/copper-key-iof.svg): This version is aligned to the [Industrial Ontologies Foundry Core Ontology](https://github.com/iofoundry/ontology/tree/master/core).

Graphs built with this ontology:

1. Browse copper-key with [Sparklis](http://sparklis.kupferdigital.org/osparklis.html?endpoint=http%3A//fuseki.kupferdigital.org/copper-key/sparql&entity_lexicon_select=http%3A//www.w3.org/2000/01/rdf-schema%23label&concept_lexicons_select=http%3A//www.w3.org/2000/01/rdf-schema%23label).
1. Query copper-key with [Fuseki](http://fuseki.kupferdigital.org/dataset.html?tab=query&ds=/copper-key).
1. Query copper-key-iof with [Fuseki](http://fuseki.kupferdigital.org/dataset.html?tab=query&ds=/copper-key-iof).
